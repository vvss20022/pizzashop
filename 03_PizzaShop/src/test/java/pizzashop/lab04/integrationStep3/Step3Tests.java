package pizzashop.lab04.integrationStep3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

public class Step3Tests {
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(null, paymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_getTotalAmount_WithCashValues_Integration() throws Exception {
        //Arrange
        Payment p1 = new Payment(3, PaymentType.Cash, 2);
        Payment p2 = new Payment(3, PaymentType.Card, 5);
        Payment p3 = new Payment(3, PaymentType.Cash, 5);

        //Act
        pizzaService.addPayment(p1);
        pizzaService.addPayment(p2);
        pizzaService.addPayment(p3);
        double result = pizzaService.getTotalAmount(PaymentType.Cash);

        //Assert
        assertEquals(7, result);
    }

    @Test
    void addPayment_getTotalAmount_WithoutCashValues_Integration() throws Exception {
        //Arrange
        Payment p1 = new Payment(3, PaymentType.Card, 2);
        Payment p2 = new Payment(3, PaymentType.Card, 5);
        Payment p3 = new Payment(3, PaymentType.Card, 5);

        //Act
        pizzaService.addPayment(p1);
        pizzaService.addPayment(p2);
        pizzaService.addPayment(p3);
        double result = pizzaService.getTotalAmount(PaymentType.Cash);

        //Assert
        assertEquals(0, result);
    }
}
