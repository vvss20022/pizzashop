package pizzashop.lab04.unittests;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Any;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.repository.mocks.MockPaymentRepository;
import pizzashop.service.PizzaService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PizzaServiceTest {
    private PaymentRepository mockitoPaymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        mockitoPaymentRepository = mock(PaymentRepository.class);
        pizzaService = new PizzaService(null, mockitoPaymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_When_ValidInput_PayRepo_Add_IsCalledOnce() throws Exception {
        //Arrange

        //Act
        pizzaService.addPayment(4, PaymentType.Card, 10);

        //Assert
        Mockito.verify(mockitoPaymentRepository, Mockito.times(1)).add(any());
    }

    @Test
    void addPayment_When_Table_LessThan1_ThrowsException() {
        //Arrange

        //Act
        Exception exception = assertThrows(Exception.class, () -> {
            pizzaService.addPayment(-2, PaymentType.Card, 10);
        });

        //Assert
        assertEquals("Invalid table number", exception.getMessage());
        Mockito.verify(mockitoPaymentRepository, Mockito.times(0)).add(any());
    }
}