package pizzashop.lab04.unittests;

import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class PaymentTest {
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void get_set_tableNumber() {
        //Arrange
        Payment payment = new Payment(1,PaymentType.Card, 1);

        //Act
        int originalTableNumber = payment.getTableNumber();
        payment.setTableNumber(2);
        int modifiedTableNumber = payment.getTableNumber();

        //Assert
        assertEquals(1, originalTableNumber);
        assertEquals(2, modifiedTableNumber);
    }

    @Test
    void get_set_type() {
        //Arrange
        Payment payment = new Payment(1,PaymentType.Card, 1);

        //Act
        PaymentType originalType = payment.getType();
        payment.setType(PaymentType.Cash);
        PaymentType modifiedType = payment.getType();

        //Assert
        assertEquals(PaymentType.Card, originalType);
        assertEquals(PaymentType.Cash, modifiedType);
    }
}