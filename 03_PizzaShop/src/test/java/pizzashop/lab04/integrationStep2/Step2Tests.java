package pizzashop.lab04.integrationStep2;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;

class Step2Tests {
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @BeforeEach
    void setUp() {
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(null, paymentRepository);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void addPayment_When_ValidInput_Payment_IsAdded() throws Exception {
        //Arrange
        Payment payment = mock(Payment.class);
        Mockito.when(payment.getTableNumber()).thenReturn(3);

        //Act
        pizzaService.addPayment(payment);
        pizzaService.addPayment(payment);
        List<Payment> payments = pizzaService.getPayments();

        //Assert
        assertEquals(2, payments.size());
    }

    @Test
    void addPayment_When_Table_LessThan1_Payment_IsNotAdded() throws Exception {
        //Arrange
        Payment payment = mock(Payment.class);
        Mockito.when(payment.getTableNumber()).thenReturn(-3);

        //Act
        Exception exception = assertThrows(Exception.class, () -> {
            pizzaService.addPayment(payment);
        });

        List<Payment> payments = pizzaService.getPayments();

        //Assert
        assertEquals("Invalid table number", exception.getMessage());
        assertEquals(0, payments.size());
    }
}