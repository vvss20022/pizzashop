package pizzashop.repository.mocks;

import pizzashop.model.Payment;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class MockPaymentRepository extends PaymentRepository {
    public int addMethodCalledTimes = 0;
    public List<Payment> getAllResult;

    public MockPaymentRepository() {
    }

    public MockPaymentRepository(List<Payment> getAllResult) {
        this.getAllResult = getAllResult;
    }

    public void add(Payment payment){
        addMethodCalledTimes++;
    }

    public List<Payment> getAll(){
        return getAllResult;
    }

}
